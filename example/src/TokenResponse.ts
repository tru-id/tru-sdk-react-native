export type TokenResponse = {
  token: string;
  url: string;
};
